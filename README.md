# Api Dogs

![Api Dogs Logo](https://dog.ceo/img/dog-api-logo.svg)

This app uses the [Api Dogs](https://dog.ceo/), so we can display a random image from this repository.

Cristhian Bacusoy &copy; 2022
