import 'dart:convert';

Dog dogFromJson(String str) => Dog.fromJson(json.decode(str));

String dogToJson(Dog data) => json.encode(data.toJson());

class Dog {
  String message;
  String status;

  Dog({
    required this.message,
    required this.status,
  });

  factory Dog.fromJson(Map<String, dynamic> json) => Dog(
    message: json["message"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "status": status,
  };
}
