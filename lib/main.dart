import 'package:api_dog/models/dog.dart';
import 'package:api_dog/screens/home.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

Dog test = Dog(
  message: "https://images.dog.ceo/breeds/saluki/n02091831_2995.jpg", 
  status: "correct",
);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomeScreen(),
    );
  }
}

