import 'package:flutter/foundation.dart';
import 'package:api_dog/models/dog.dart';
import 'package:http/http.dart' as http;

class DogApi {
  String urlBase = "https://dog.ceo/api";
  String endPoint = "/breeds/image/random";

  Future<Dog> getDog() async {
    final http.Response response = await http.get(Uri.parse(urlBase + endPoint));

    return await compute(dogFromJson, response.body);
  }
}
