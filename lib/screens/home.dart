import 'package:api_dog/apis/dog_api.dart';
import 'package:flutter/material.dart';
import 'package:api_dog/models/dog.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String imageUri = "https://www.vuescript.com/wp-content/uploads/2018/11/Show-Loader-During-Image-Loading-vue-load-image.png";
  bool isfirstLoad = true;

  void fetchDog() async {
    Dog newDogInfo = await DogApi().getDog();
    setState(() {
      imageUri = newDogInfo.message;
      isfirstLoad = false;
    });    
  }

  @override
  Widget build(BuildContext context) {
    if (isfirstLoad) fetchDog();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Dogs App"),
        centerTitle: true,
        backgroundColor: Colors.black87,
      ),
      backgroundColor: const Color.fromARGB(255, 26, 26, 26),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.7,
              child: Image.network(imageUri),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: fetchDog, 
              child: const Text("New Dog"),
            ),
          ],
        ),
      ),
    );
  }
}